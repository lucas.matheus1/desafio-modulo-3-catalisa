package DesafioMod3.dominio;

import java.util.Scanner;

public class IO {

    //Metódo para mostrar uma mensagem para o usuário, guiar o mesmo.
    public static void mostrarComentario(String Comentario){

        System.out.println(Comentario);
    }

    //Metódo para Instanciar o Scanner para chamar na minha class Sistema.
    public static Scanner usuarioDigita(){

        return new Scanner(System.in);

    }

}
