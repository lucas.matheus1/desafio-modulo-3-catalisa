package DesafioMod3.dominio;


public class Pessoa {

    //Atributos...
    private String nome, telefone, cpf;

    //Metódo Construtor...
    public Pessoa(){

    }

    public Pessoa(String nome, String telefone, String cpf) {
        this.nome = nome;
        this.telefone = telefone;
        this.cpf = cpf;
    }

    //Metódos Seletores e Modificadores...
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }


    @Override
    public String toString() {

        System.out.println("Nome: " + nome);
        System.out.println("Telefone: " + telefone);
        System.out.println("CPF: " + cpf);
        return "";
    }
}
