package DesafioMod3.sistemaGerenciamento;

import DesafioMod3.dominio.Imovel;

import java.util.ArrayList;
import java.util.List;

public class Imobiliaria {

    //Atributos...
    private String nome;
    private  List<Imovel> listaDeImoveis = new ArrayList<>();


    //Metódo construtor...
    public Imobiliaria(){}

    public Imobiliaria(String nome) {
        this.nome = nome;

    }

    //Metódos seletores e modificadores...
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }


    //Metódo para adicionar o Imovel.
    public  void adicionarImovel(Imovel imovel){
        System.out.println("-----------------------------");
        System.out.println("Imovel cadastrado com sucesso.");
        System.out.println("-----------------------------");
        listaDeImoveis.add(imovel);
    }

    //Metódo para mostrar a lista de imovel.
    public void mostrarListaDeImovel() {
        System.out.println("Lista de Imoveis Para Vendas ou Aluguel: ");
        System.out.println(" ");
        for (int i = 0; i < listaDeImoveis.size(); i++) {

            System.out.println(listaDeImoveis.get(i));
        }
    }

    //Metódo para encontrar o imovel onde uso o throws.
    public Imovel encontrarImovel(String endereco) throws Exception{

        for (int i = 0; i < listaDeImoveis.size(); i++) {

            if (listaDeImoveis.get(i).getEndereco().equals(endereco)) {

                return listaDeImoveis.get(i);
            }

        }
        throw new Exception("Imovel não encontrado. ");

    }

    @Override
    public String toString() {
        System.out.println("Imobiliaria: ");
        System.out.println("Nome do imovel: " + nome);
        return " ";
    }
}
